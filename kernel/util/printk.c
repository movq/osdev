#include <stdint.h>
#include <stdarg.h>

#include "drivers/vga.h"

static void int_to_str(uint64_t n, unsigned int base, char *buf, unsigned int bufsize);

void printk(const char *format, ...)
{
    va_list parameters;
    va_start(parameters, format);

    while (*format != '\0') {
        if (*format == '%') {
            // Decimal 64-bit integer
            if (*(format+1) == 'd') {
                char buf[20];
                uint64_t n = va_arg(parameters, uint64_t);

                int_to_str(n, 10, buf, 20);
                printk(buf);
                format += 2;
                continue;
            // Hex 64-bit integer
            } else if (*(format+1) == 'x') {
                char buf[20];
                uint64_t n = va_arg(parameters, uint64_t);

                int_to_str(n, 16, buf, 20);
                printk(buf);
                format += 2;
                continue;
            // String
            } else if (*(format+1) == 's') {
                char *s = va_arg(parameters, char*);
                printk(s);
                format += 2;
                continue;
            }
        } else {
            vga_putc(*format);
        }
        format++;
    }
}

static void int_to_str(uint64_t n, unsigned int base, char *buf, unsigned int bufsize)
{
    unsigned int numdigits = 0;
    unsigned int offset = 0;
    char copy[bufsize];

    if (base == 16) {
        // for 0x prefix
        offset = 2;
        copy[0] = '0';
        copy[1] = 'x';
    }

    do {
        char digit = n % base;
        buf[numdigits] = "0123456789ABCDEF"[(unsigned char)digit];
        n /= base;
        numdigits++;
    } while (n != 0 && numdigits < bufsize);

    for (unsigned int i = numdigits; i > 0; --i) {
        copy[numdigits - i + offset] = buf[i - 1];
    }
    copy[numdigits + offset] = '\0';

    for (unsigned int i = 0; i <= numdigits + offset; ++i) {
        buf[i] = copy[i];
    }
}


