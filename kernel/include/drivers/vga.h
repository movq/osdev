#ifndef VGA_H
#define VGA_H

void vga_putc(char c);
void vga_puts(char *s);
void vga_clear();
void vga_scroll_up();

#endif
