#include "drivers/vga.h"

static char* vidmem = (char*)0xffffffff800b8000;
static int row = 0;
static int col = 0;

// Print a single character
void vga_putc(char c)
{
    if (col == 80 || c == '\n') {
        col = 0;
        row++;
    }

    if (row == 25) {
        col = 0;
        row = 24;
        vga_scroll_up();
    }

    if (c != '\n') {
        int index = (row * 80 + col) * 2;

        vidmem[index] = c;
        vidmem[index+1] = 0x07; // Light grey on black

        ++col;
    }
}

// Print a string
void vga_puts(char *s)
{
    while (*s != '\0') {
        vga_putc(*s);
        ++s;
    }
}

void vga_clear()
{
    unsigned int num_chars = 25 * 80 * 2;

    for (unsigned int i = 0; i < num_chars; i += 2) {
        vidmem[i] = ' ';
        vidmem[i+1] = 0x07;
    }
}

void vga_scroll_up()
{
    for (unsigned int line = 1; line < 25; line++) {
        // TODO: use a library function to do the copying (memcpy or something)
        // instead of manually.
        for (unsigned int i = 0; i < 160; i++) {
            vidmem[160 * (line - 1) + i] = vidmem[160 * line + i];
        }
    }
}

