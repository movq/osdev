#include <stdint.h>

#include "drivers/vga.h"
#include "util/printk.h"

// TODO: Clean up into its own file
typedef struct e820_mmap {
    uint64_t base;
    uint64_t length;
    uint32_t type;
    uint32_t acpi_ext;
} __attribute__((packed)) e820_mmap;

static void dump_mmap(uint32_t mmap_sz, e820_mmap *mmap);

void kmain(uint32_t mmap_sz, e820_mmap *mmap)
{
    vga_clear();

    // mmap points to the location of the mmap in the physical address space,
    // but we are now operating in a virtual address space so we need to add an
    // offset to the pointer.
    e820_mmap *mmap_virtual = (e820_mmap*)((uint8_t*)mmap + 0xffffffff80000000);
    dump_mmap(mmap_sz, mmap_virtual);

    while(1) {
        __asm__ __volatile__("hlt");
    }
}

static void dump_mmap(uint32_t mmap_sz, e820_mmap *mmap)
{
    const char* mem_types[] =
    {
        "Usable",
        "Reserved",
        "ACPI reclaimable",
        "ACPI NVS",
        "Bad memory"
    };

    for (unsigned int i = 0; i < mmap_sz; ++i) {
        printk("start  = %x\n", mmap[i].base);
        printk("length = %x\n", mmap[i].length);
        printk("type   = %s\n\n", mem_types[(uint64_t)mmap[i].type - 1]);
    }
}
