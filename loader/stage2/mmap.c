#include <stdint.h>

#include "mmap.h"
#include "common.h"
#include "bios.h"

unsigned int get_memory_map(void *buf, uint32_t max_entries)
{
    uint16_t seg, off;
    linear_to_segoff(buf, &seg, &off);
    return bios_memory_map(seg, off, max_entries);
}

