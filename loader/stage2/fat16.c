#include <stdint.h>
#include <stdbool.h>

#include "fat16.h"
#include "common.h"
#include "disk.h"
#include "mem.h"
#include "console.h"

static void load_bpb();
static uint16_t first_cluster_of_file(const char *filename);
static void parse_short_filename(char *filename, char *out);
static void load_cluster(uint16_t cluster, uint8_t *dest);
static void load_cluster_chain(uint16_t start_clus, int num_clusters, uint8_t *dest);
static uint16_t get_next_cluster(uint16_t clus);

static uint32_t partition_start;

static struct fat16_bpb bpb;
static struct fat16_info info;


void fat16_read_bytes(const char *path, uint8_t *buf, unsigned int offset, unsigned int n)
{
    int bytes_per_cluster = bpb.bytes_per_sec * bpb.sec_per_clus;

    // Number of clusters to be read
    int num_clusters = (n + bytes_per_cluster - 1) / bytes_per_cluster;

    int start_cluster = (offset + bytes_per_cluster - 1) / bytes_per_cluster - 1;
    // Inelegant way of dealing with offset = 0
    if (start_cluster < 0)
        start_cluster = 0;
    start_cluster += first_cluster_of_file(path);

    // 4. Read those clusters into a buffer.
    uint8_t *cluster_buffer = malloc(num_clusters * bytes_per_cluster);
    load_cluster_chain(start_cluster, num_clusters, cluster_buffer);

    int cluster_offset = offset % bytes_per_cluster;

    // 5. Copy the part of that buffer containing the bytes we want into buf.
    memcpy(buf, cluster_buffer + cluster_offset, n);

    free(cluster_buffer);

}

void init_fat16(uint32_t partition_start_sec)
{
    partition_start = partition_start_sec;

    load_bpb();

    info.start_of_fat = bpb.rsvd_sec_cnt;
    info.start_of_rootdir = bpb.rsvd_sec_cnt + bpb.num_fats * bpb.fat_size16;
    info.start_of_data = info.start_of_rootdir + bpb.root_ent_cnt * 32 / bpb.bytes_per_sec;
}

static uint16_t first_cluster_of_file(const char *filename)
{
    // Number of sectors occupied by the root directory.
    unsigned int root_dir_sectors = (bpb.root_ent_cnt * 32 + bpb.bytes_per_sec - 1) / bpb.bytes_per_sec;
    uint8_t *buf = malloc(root_dir_sectors * bpb.bytes_per_sec);
    uint8_t *cur = buf;

    read_sectors(partition_start + info.start_of_rootdir, root_dir_sectors, buf);

    while (1) {
        bool allzero = true;

        for (uint8_t *p = cur; p <= cur + 16; ++p) {
            if (*p != 0) {
                allzero = false;
                break;
            }
        }

        if (allzero) return 0;

        char cur_fn[13];
        parse_short_filename((char*)cur, cur_fn);
        
        if (!strcmp(cur_fn, filename)) {
            return get_u16(cur + 26);
        }

        cur += 32;
    }


    free(buf);
    return 0;
}

static void load_bpb()
{
    uint8_t buf[512];

    read_sectors(partition_start, 1, buf);

    bpb.bytes_per_sec = get_u16(buf + FAT16_BYTES_PER_SEC);
    bpb.sec_per_clus = get_u8(buf + FAT16_SEC_PER_CLUS);
    bpb.rsvd_sec_cnt = get_u16(buf + FAT16_RSVD_SEC_CNT);
    bpb.num_fats = get_u8(buf + FAT16_NUM_FATS);
    bpb.root_ent_cnt = get_u16(buf + FAT16_ROOT_ENT_CNT);
    bpb.tot_sec16 = get_u16(buf + FAT16_TOT_SEC16);
    bpb.fat_size16 = get_u16(buf + FAT16_FAT_SIZE16);
    bpb.tot_sec32 = get_u32(buf + FAT16_TOT_SEC32);
}

static void parse_short_filename(char *filename, char *out)
{
    int fn_len = 0;
    int ext_len = 0;

    for (fn_len = 0; fn_len < 8 && filename[fn_len] != ' '; fn_len++);
    fn_len++;
    strncpy(out, filename, fn_len);
    out[fn_len - 1] = '.';

    for (int j = 10; j >= 8 && filename[j] != ' '; j--, ext_len++);
    strncpy(out + fn_len, filename + 11 - ext_len, ext_len);
    out[fn_len + ext_len] = '\0';

}

static void load_cluster(uint16_t cluster, uint8_t *dest)
{
    uint32_t first_sector = partition_start + info.start_of_data + (cluster - 2) * bpb.sec_per_clus;
    read_sectors(first_sector, bpb.sec_per_clus, dest);
}

static void load_cluster_chain(uint16_t start_clus, int num_clusters, uint8_t *dest)
{
    uint8_t *cur_dest = dest;
    uint16_t cur_clus = start_clus;

    uint8_t *buf = malloc(bpb.sec_per_clus * bpb.bytes_per_sec);

    while (cur_clus != 0xFFFF && num_clusters > 0) {
        load_cluster(cur_clus, buf);

        memcpy(cur_dest, buf, bpb.sec_per_clus * bpb.bytes_per_sec);

        cur_clus = get_next_cluster(cur_clus);
        cur_dest += bpb.sec_per_clus * bpb.bytes_per_sec;
        num_clusters--;
    }

    free(buf);
}

static uint16_t get_next_cluster(uint16_t clus)
{
    uint8_t *buf = malloc(bpb.fat_size16 * bpb.bytes_per_sec);

    read_sectors(partition_start + info.start_of_fat, bpb.fat_size16, buf);

    uint16_t next = get_u16(buf + clus * 2);
    free(buf);
    return next;
}

