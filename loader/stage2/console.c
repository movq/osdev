#include <stdint.h>

#include "console.h"
#include "common.h"
#include "bios.h"

void putchar(char c)
{
    bios_putchar(c);
}

void puts(char *s)
{
    uint16_t seg = 0;
    uint16_t off = 0;

    linear_to_segoff(s, &seg, &off);
    bios_puts(seg, off);
}
