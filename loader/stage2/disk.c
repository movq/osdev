#include <stdint.h>

#include "disk.h"
#include "common.h"
#include "bios.h"

extern uint16_t dap_num_sects;
extern uint16_t dap_offset;
extern uint16_t dap_segment;
extern uint64_t dap_start_sect;

int read_sectors(uint64_t lba, uint32_t count, uint8_t *dest)
{
    dap_start_sect = lba;
    dap_num_sects = count;

    linear_to_segoff(dest, &dap_segment, &dap_offset);

    bios_read_sectors();

    return 0;
}

