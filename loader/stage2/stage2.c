#include <stdint.h>

#include "mem.h"
#include "console.h"
#include "partition.h"
#include "fat16.h"
#include "elf.h"
#include "common.h"
#include "mmap.h"
#include "interrupts.h"

extern char stage2_end_addr[];

void cmain(void)
{
    init_idt();
    puts("stage 2 running in protected mode!\r\n");

    init_heap(stage2_end_addr);

    uint32_t partition_start = get_partition_start_sector();
    init_fat16(partition_start);

    // Load the kernel
    load_elf64_kernel("KERNEL.ELF");

    // Prepare a memory map.
    void *mmap = malloc(24 * 20);   // Reserve space for at most 20 24-byte entries
    uint32_t mmap_sz = get_memory_map(mmap, 20);

    __asm__ __volatile__("ljmp $0x8, $0x100000"
                         :
                         :"a"(mmap_sz), "b"(mmap)
                         :
                        );
}

