#include <stdint.h>

#include "mem.h"
#include "common.h"

typedef struct header {
    struct header *ptr;
    unsigned int size;
} header;

static header *start_of_free;

// The following implementations of malloc and free are extremely naive, with
// many issues. For example, if a block to be freed is surrounded by free space
// on either side, the two free spaces will not be merged.
void *malloc(unsigned int size)
{
    header *p = start_of_free;

    unsigned int blocks = (size + sizeof(header) - 1) / sizeof(header);
    
    do {
        if (p->size - 1 >= blocks) {
            p->size -= blocks + 1;
            (p + p->size + 1)->size = blocks;
            return p + p->size + 2;
        }
        p = p->ptr;
    } while (p->ptr != start_of_free);

    return 0;
}

void free(void *ptr)
{
    header *p = start_of_free;
    header *last = 0;
    header *blockstart = (header*)ptr - 1;

    while (!(p < blockstart && blockstart < p->ptr)) {
        // Do this loop until blockstart is 'in between' p and p->ptr, i.e.,
        // until after p is where the freed region should be inserted.
        if (p->ptr == start_of_free)
            break;

        p = p->ptr;
    }

    header *pnext = p->ptr;

    // If the block is immediately after a free block, merge the two into a
    // single block.
    if (p + p->size + 1 == blockstart) {
        p->size += blockstart->size + 1;
        // If the block is also followed by a free block, merge the two free
        // blocks.
        if (p + p->size + 1 == pnext) {
            p->size += pnext->size + 1;
            p->ptr = pnext->ptr;
        }
    // If the block is immediately before a free block, merge the two into a
    // single block.
    } else if (blockstart + blockstart->size + 1 == pnext) {
        blockstart->size += pnext->size + 1;
        blockstart->ptr = pnext->ptr;
        p->ptr = blockstart;
    // Otherwise, just merge it into the linked list.
    } else {
        p->ptr = blockstart;
        blockstart->ptr = pnext;
    }
}

void init_heap(void *start)
{
    uint32_t start_addr = (uint32_t)start;
    // Align the start address
    start_addr += 0x100;
    start_addr &= 0xFFFFFF00;

    start_of_free = (header*)start_addr;
    // This is the last free block so point to ourself.
    start_of_free->ptr = start_of_free;
    // 100 KiB heap
    start_of_free->size = 0x19000 / sizeof(header);
}

