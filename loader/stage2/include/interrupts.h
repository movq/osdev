#ifndef INTERRUPTS_H
#define INTERRUPTS_H

#include <stdint.h>

typedef struct
{
	uint16_t base_lower;	// Lower 16 bits of handler
	uint16_t sel; // Kernel segment selector
	uint8_t always_0;
	uint8_t flags;
	uint16_t base_higher;
} __attribute__((packed)) idt_entry_t;

typedef struct
{
	uint16_t limit;
	uint32_t base;
} __attribute__((packed)) idt_ptr_t;

void init_idt();

#endif
