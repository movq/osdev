#ifndef COMMON_H
#define COMMON_H

#include <stdint.h>

inline uint8_t get_u8(void *p)
{
    return *((uint8_t*)p);
}

inline uint16_t get_u16(void *p)
{
    return *((uint16_t*)p);
}

inline uint32_t get_u32(void *p)
{
    return *((uint32_t*)p);
}

inline uint64_t get_u64(void *p)
{
    return *((uint64_t*)p);
}

void printf(const char *format, ...);

void linear_to_segoff(void *ptr, uint16_t *seg, uint16_t *off);

int strcmp(const char *s1, const char *s2);
char *strncpy(char *dest, const char *src, unsigned int n);
void *memcpy(void *dest, const void *src, unsigned int n);
void *memset(void *s, uint8_t c, unsigned int n);

#endif

