#ifndef DISK_H
#define DISK_H

int read_sectors(uint64_t lba, uint32_t count, uint8_t *dest);

#endif

