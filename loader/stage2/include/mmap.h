#ifndef MMAP_H
#define MMAP_H

unsigned int get_memory_map(void *buf, uint32_t max_entries);

#endif

