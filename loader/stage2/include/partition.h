#ifndef PARTITION_H
#define PARTITION_H

#include <stdint.h>

uint32_t get_partition_start_sector();

#endif
