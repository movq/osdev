#ifndef FAT16_H
#define FAT16_H

#define FAT16_BYTES_PER_SEC 11
#define FAT16_SEC_PER_CLUS 13
#define FAT16_RSVD_SEC_CNT 14
#define FAT16_NUM_FATS 16
#define FAT16_ROOT_ENT_CNT 17
#define FAT16_TOT_SEC16 19
#define FAT16_FAT_SIZE16 22
#define FAT16_TOT_SEC32 32

void fat16_read_bytes(const char *path, uint8_t *buf, unsigned int offset, unsigned int n);
void init_fat16(uint32_t partition_start_sec);

struct fat16_bpb {
    uint16_t bytes_per_sec;
    uint8_t sec_per_clus;
    uint16_t rsvd_sec_cnt;
    uint8_t num_fats;
    uint16_t root_ent_cnt;
    uint16_t tot_sec16;
    uint16_t fat_size16;
    uint32_t tot_sec32;
};

struct fat16_info {
    uint32_t start_of_fat;
    uint32_t start_of_rootdir;
    uint32_t start_of_data;
};

#endif

