#ifndef MEM_H
#define MEM_H

#include <stdint.h>

void *malloc(unsigned int size);
void free(void *ptr);
void init_heap(void *start);

#endif

