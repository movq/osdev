#ifndef BIOS_H
#define BIOS_H

#include <stdint.h>

void bios_putchar(char c);
void bios_puts(uint16_t seg, uint16_t off);

int bios_read_sectors();

uint16_t bios_memory_map(uint16_t seg, uint16_t off, uint32_t max);

#endif

