#include <stdint.h>
#include <stdarg.h>

#include "common.h"
#include "console.h"

static void int_to_str(unsigned int n, unsigned int base, char *buf, unsigned int bufsize);


void printf(const char *format, ...)
{
    va_list parameters;
    va_start(parameters, format);

    while (*format != '\0') {
        if (*format == '%') {
            if (*(format+1) == 'd') {
                char buf[20];
                unsigned int n = va_arg(parameters, unsigned int);

                int_to_str(n, 10, buf, 20);
                printf(buf);
                format += 2;
                continue;
            } else if (*(format+1) == 'x') {
                char buf[20];
                unsigned int n = va_arg(parameters, unsigned int);

                int_to_str(n, 16, buf, 20);
                printf(buf);
                format += 2;
                continue;
            }
        } else if (*format == '\n') {
            putchar('\r');
            putchar('\n');
        } else {
            putchar(*format);
        }
        format++;
    }
}

static void int_to_str(unsigned int n, unsigned int base, char *buf, unsigned int bufsize)
{
    unsigned int numdigits = 0;
    unsigned int offset = 0;
    char copy[bufsize];

    if (base == 16) {
        // for 0x prefix
        offset = 2;
        copy[0] = '0';
        copy[1] = 'x';
    }

    do {
        char digit = n % base;
        buf[numdigits] = "0123456789ABCDEF"[(unsigned char)digit];
        n /= base;
        numdigits++;
    } while (n != 0 && numdigits < bufsize);

    for (unsigned int i = numdigits; i > 0; --i) {
        copy[numdigits - i + offset] = buf[i - 1];
    }
    copy[numdigits + offset] = '\0';

    for (unsigned int i = 0; i <= numdigits + offset; ++i) {
        buf[i] = copy[i];
    }
}

// Convert a 32-bit absolute address into an equivalent segment:offset.
void linear_to_segoff(void *ptr, uint16_t *seg, uint16_t *off)
{
    uint32_t linear = (uint32_t)ptr;
    uint32_t seg_start;

    // Round the linear address down to a multiple of 16 to get the segment.
    seg_start = linear & 0xFFFF0;

    // Subtract the segment from the linear address to get the offset.
    *off = linear - seg_start;

    // Divide by 16 to get segment selector
    *seg = seg_start >> 4;
}

int strcmp(const char *s1, const char *s2)
{
    while(*s1 != '\0' && *s2 != '\0') {
        if (*s1 > *s2)
            return 1;
        else if (*s1 < *s2)
            return -1;

        s1++;
        s2++;
    }

    if (*s1 == '\0' && *s2 == '\0')
        return 0;
    else if (*s1 == '\0')
        return -1;
    else
        return 1;

}

char *strncpy(char *dest, const char *src, unsigned int n)
{
    unsigned int i = 0;

    for (i = 0; i < n && src[i] != '\0'; i++) {
        dest[i] = src[i];
    }

    if (i < n) {
        dest[i] = '\0';
    }

    return dest;
}

void *memcpy(void *dest, const void *src, unsigned int n)
{
    for (unsigned int i = 0; i < n; ++i) {
        ((uint8_t*)dest)[i] = ((uint8_t*)src)[i];
    }
    return dest;
}

void *memset(void *s, uint8_t c, unsigned int n)
{
    for (unsigned int i = 0; i < n; ++i) {
        ((uint8_t*)s)[i] = c;
    }
    return s;
}
