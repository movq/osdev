#include <stdint.h>

#include "partition.h"
#include "disk.h"
#include "common.h"

uint32_t get_partition_start_sector()
{
    unsigned char mbr[512];
    int index = 0x1be;  // The offset of the first partition table entry.

    // Read the bootsector of the current drive.
    read_sectors(0, 1, mbr);

    for (unsigned int index = 0x1be; index <= 0x1ee; index += 0x10) {
        unsigned char partition_status = mbr[index];

        // Bit 7 is set if partition is active.
        if (partition_status & 0x80) {
            return get_u32(mbr + index + 0x8);
        }

    }

    return 0;
}

