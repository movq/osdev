#include <stdint.h>

#include "elf.h"
#include "common.h"
#include "fat16.h"

#define EI_NIDENT 16

typedef struct {
    unsigned char   e_ident[EI_NIDENT];
    uint16_t        e_type;
    uint16_t        e_machine;
    uint32_t        e_version;
    uint64_t        e_entry;
    uint64_t        e_phoff;
    uint64_t        e_shoff;
    uint32_t        e_flags;
    uint16_t        e_ehsize;
    uint16_t        e_phentsize;
    uint16_t        e_phnum;
    uint16_t        e_shentsize;
    uint16_t        e_shnum;
    uint16_t        e_shstrndx;
} elf64_hdr;

typedef struct {
    uint32_t        p_type;
    uint32_t        p_flags;
    uint64_t        p_offset;
    uint64_t        p_vaddr;
    union {
        uint64_t        p_paddr;
        struct {
            uint32_t    p_paddr_lower;
            uint32_t    p_paddr_upper;
        } p;
    };
    uint64_t        p_filesz;
    uint64_t        p_memsz;
    uint64_t        p_align;
} elf64_phdr;

static void load_segment(const char *path, elf64_phdr *phdr);

void *load_elf64_kernel(const char *path)
{
    elf64_hdr *hdr;
    elf64_phdr *phdr;

    uint8_t header_buf[512];

    fat16_read_bytes(path, header_buf, 0, 512);

    hdr = (elf64_hdr*)header_buf;

    for (unsigned int i = 0; i < hdr->e_phnum; i++) {
        phdr = (elf64_phdr*)(header_buf + hdr->e_phoff + i * hdr->e_phentsize);
        load_segment(path, phdr);
    }

    return (void*)0x100000;
}

static void load_segment(const char *path, elf64_phdr *phdr) {
    int bytes_loaded = 0;
    int bytes_left = phdr->p_filesz;
    uint8_t *dest = (uint8_t*)phdr->p.p_paddr_lower;

    while (bytes_left > 0) {
        uint8_t buf[512];
        int bytes_read;

        if (bytes_left < 512) {
            fat16_read_bytes(path, buf, phdr->p_offset + bytes_loaded, bytes_left);
            bytes_read = bytes_left;
            bytes_left = 0;
        } else {
            fat16_read_bytes(path, buf, phdr->p_offset + bytes_loaded, 512);
            bytes_read = 512;
            bytes_left -= 512;
        }

        memcpy(dest + bytes_loaded, buf, bytes_read);
        bytes_loaded += bytes_read;
    }

    int bss_size = phdr->p_memsz - phdr->p_filesz;
    memset(dest + bytes_loaded, 0, bss_size);
}

