# Main makefile
include include.mk

# Get the current user
USER = $(shell whoami)

.PHONY: all clean image

all:
	$(Q) make -C loader
	$(Q) make -C kernel

image: hdd.img

hdd.img: all
	sudo util/gen_disk.sh
	sudo chown $(USER):$(USER) hdd.img

qemu: image
	qemu-system-x86_64 -drive file=hdd.img,format=raw,index=0,media=disk

vbox: image
	if [ -f "hdd.vdi" ] ; then rm hdd.vdi; fi
	VBoxManage convertdd hdd.img hdd.vdi --format VDI --uuid 6414f01c-cca4-403f-bb05-d5a56a3dd258

clean:
	$(Q) make -C loader clean
	$(Q) make -C kernel clean
	$(E) " CLEAN   "
	$(Q) rm -rf hdd.img hdd.vdi mnt

