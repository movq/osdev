#!/bin/bash

if [ "$(whoami)" != "root" ]; then
    echo "This script must be run as root."
    exit 1
fi

LOOPDEV=$(losetup -f)
PARTITION=/dev/mapper/$(basename $LOOPDEV)p1

# Create a blank image
fallocate -l 10M hdd.img

# Create the partition table
fdisk hdd.img >/dev/null 2>&1 << EOF
o
n
p
1


a
w
q
EOF

# Set up the loopback device
kpartx -av hdd.img
sleep 0.5s

# Format the partition
mkfs.vfat $PARTITION

if [ ! -d mnt ]; then
    mkdir mnt
fi

# Mount the partition
mount $PARTITION mnt

python3 util/install.py

# Clean up
sleep 0.5s
kpartx -d hdd.img

