#!/usr/bin/env python

import subprocess
import os
import sys
import struct


STAGE2_LIST_OFFSET = 0x174

def get_sector_list():
    hdparm = subprocess.check_output(["hdparm", "--fibmap",
        "mnt/STAGE2.BIN"]).decode("utf-8")

    lines = []

    for line in hdparm.split('\n'):
        line = line.lstrip()
        if((len(line)) and line[0].isdigit()):
            lines.append(list(map(int, line.split())))

    return lines

def get_partition_start():
    fdisk = subprocess.check_output(["fdisk", "-l", "hdd.img"]).decode("utf-8")

    for line in fdisk.split('\n'):
            line = line.split()
            if len(line) and line[0] == "hdd.img1":
                return int(line[2])

def install_mbr():
    # Read the mbr
    mbr_file = open('loader/mbr/mbr.bin', 'rb')
    mbr = mbr_file.read(446)
    mbr_file.close()

    disk_file = open('hdd.img', 'r+b')
    disk_file.write(mbr)
    disk_file.close()

def install_stage1(offset):
    stage1_file = open('loader/stage1/stage1.bin', 'rb')
    stage1 = stage1_file.read(512)
    stage1_file.close()

    disk_file = open('hdd.img', 'r+b')

    # First, write the first three bytes (the JMP and NOP).
    disk_file.seek(offset * 512)
    disk_file.write(stage1[:3])

    # Next, write the bootloader code after the FAT info.
    disk_file.seek(offset * 512 + 90)
    disk_file.write(stage1[90:])

    disk_file.close()

def install_files():
    os.system("cp loader/stage2/stage2.bin mnt/STAGE2.BIN")
    os.system("cp kernel/kernel.elf mnt/KERNEL.ELF")

def patch_stage1(partition_start, stage2_first_sector):
    data = struct.pack('L', stage2_first_sector)

    disk_file = open('hdd.img', 'r+b')

    disk_file.seek(partition_start * 512 + 0x5a)
    disk_file.write(data)
    disk_file.close()

def patch_stage2(partition_start, first_sector, sector_list):
    disk_file = open('hdd.img', 'r+b')
    current_offset = 512 * first_sector + STAGE2_LIST_OFFSET
    first = True

    for entry in sector_list:
        if first:
            # We don't want the loader to read the first sector again.
            data = struct.pack('L', partition_start + entry[1] + 1) + struct.pack('H', entry[3] - 1)
            first = False
        else:
            data = struct.pack('L', partition_start + entry[1]) + struct.pack('H', entry[3])

        disk_file.seek(current_offset)
        disk_file.write(data)
        current_offset += 10

    disk_file.close()

if __name__ == '__main__':
    partition_start = get_partition_start()
    install_files()
    sector_list = get_sector_list()
    first_sector = partition_start + sector_list[0][1]

    os.system("umount mnt")
    install_mbr()
    install_stage1(partition_start)

    patch_stage1(partition_start, first_sector)
    patch_stage2(partition_start, first_sector, sector_list)

