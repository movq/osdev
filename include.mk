# Build variables
AS = x86_64-elf-as
LD = x86_64-elf-ld
CC = x86_64-elf-gcc
CFLAGS = -std=gnu99 -Wall -Wextra -Werror -Wno-unused-parameter -Wno-unused-variable -ffreestanding -m32 -O2
ASFLAGS = -m32
INCLUDE =

Q = @
E = @echo

# Generic rules
%.o: %.S
	$(E) " AS      " $@
	$(Q) $(CC) $(ASFLAGS) -c -o $@ $<

%.o: %.c
	$(E) " CC      " $@
	$(Q) $(CC) $(CFLAGS) $(INCLUDE) -c -o $@ $<
