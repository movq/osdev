osdev
=====

This is an attempt at writing an x86 operating system, for educational
purposes.

It is ISC-licensed.

Current State
-------------

The bootloader is able to locate, load, and jump to, the kernel from the root
directory of a FAT16-formatted drive with only 8.3 filenames.

The kernel can just do rudimentary text printing.

Current Goals
-------------

1. Have the kernel's start code set up paging, enter long mode, and call the
   kmain C function.

2. Some initally rudimentary memory management including physical page
   allocation, a kernel heap, etc.

3. A simple PS/2 keyboard driver.

